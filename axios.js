import Axios from "axios";

const instance = Axios.create({
  baseURL: "https://tnfb-84dae.firebaseio.com/"
});

export default instance;
