import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
	static async getInitialProps(ctx) {
		const initialProps = await Document.getInitialProps(ctx)
		return { ...initialProps }
	}

	render() {
		return (
			<Html lang="en" className="leading-normal">
				<Head>
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<meta name="application-name" content="Bucu" />
					<meta name="description" content="Tropical Nomad food bundle tracker" />
					<meta name="theme-color" content="#ff6600" />
					<meta name="apple-mobile-web-app-title" content="TN FB" />
					<meta name="msapplication-starturl" content="/" />
					<meta name="apple-mobile-web-app-status-bar-style" content="default" />
					<meta name="apple-mobile-web-app-capable" content="yes" />
					<meta name="mobile-web-app-capable" content="yes" />
					{/* <link rel="shortcut icon" href="/static/icon-192.png" /> */}
					{/* <link rel="apple-touch-icon" href="/static/icon-192.png" /> */}
					{/* <link rel='stylesheet' href='/_next/static/style.css' /> */}{' '}
					{/* <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato:400|Roboto:300,400,500,700&display=swap' /> */}
					{/* <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap' />
					<link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons' /> */}
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		)
	}
}
