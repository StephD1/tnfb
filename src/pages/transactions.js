import React, { useEffect, useState } from 'react'
import Head from '../components/head'
import Nav from '../components/nav'
import Modal from 'react-modal'
Modal.setAppElement('#__next')
import PayWithBundleForm from '../components/transactions/PayWithBundleForm'
import DateComponent from '../components/transactions/DateComponent'
import moment from 'moment'

const customStyles = {
	content: {
		top: '40%',
		position: 'relative',
		overflow: 'hidden !important',
		left: '50%',
		right: '20%',
		bottom: 'auto',
		width: '80%',
		// marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
	},
}

const Transactions = (props) => {
	// console.log(props)
	const [transactions, setTransactions] = useState([])
	const [refreshTransactions, setRefreshTransactions] = useState(true)
	const [refreshFoodBundleList, setRefreshFoodBundleList] = useState(true)
	const [foodBundleList, setFoodBundleList] = useState([])

	const [isModalOpen, setIsModalOpen] = useState(false)
	// const [selectedFoodBundle, setSelectedFoodBundle] = useState(null)
	const [selectedTransaction, setSelectedTransaction] = useState(null)
	const [modalAmountToPay, setModalAmountToPay] = useState(null)
	const [modalTransactionIdToPay, setModalTransactionIdToPay] = useState(null)
	const [modalAction, setModalAction] = useState(null)

	const [selectedDate, setSelectedDate] = useState(moment().format('YYYYMMDD'))
	const [selectedDateDayToSub, setSelectedDateDayToSub] = useState(0)
	const [lastTransactionTime, setLastTransactionTime] = useState(undefined)

	const [loading, setLoading] = useState(true)
	const [formLoading, setFormLoading] = useState(false)

	useEffect(() => {
		if (!refreshTransactions) return
		async function getTransactions() {
			const selectedDate = moment()
			if (selectedDateDayToSub) selectedDate.subtract(selectedDateDayToSub, 'd')
			const date = selectedDate.format('YYYY-MM-DD')
			const res = await fetch(`/api/orders/getOrders?date=${date}`)
			const newObj = await res.json()
			// console.log("here", arrayOfObj);
			newObj.sort((a, b) => (a.created_at > b.created_at ? -1 : a.created_at < b.created_at ? 1 : 0))
			setTransactions(newObj)
			if (newObj.length === 0) setLastTransactionTime('')
			else if (newObj[0]) setLastTransactionTime(newObj[0].created_at)
			setLoading(false)
			setRefreshTransactions(false)
		}
		setLoading(true)
		getTransactions()
	}, [refreshTransactions])

	useEffect(() => {
		if (!refreshFoodBundleList) return
		const res = fetch('/api/foodbundles/getFoodBundles?orderBy="active"&equalTo=true').then(async (ret) => {
			const newObj = await ret.json()
			newObj.sort((a, b) =>
				a.firstname.toLowerCase() < b.firstname.toLowerCase()
					? -1
					: a.firstname.toLowerCase() > b.firstname.toLowerCase()
					? 1
					: 0,
			)
			// console.log('BundleList has been refreshed')
			setFoodBundleList(newObj)
			setRefreshFoodBundleList(false)
		})
	}, [refreshFoodBundleList])

	const resetState = () => {
		// setSelectedFoodBundle(null)
		setFormLoading(false)
		setModalAmountToPay(null)
		setModalTransactionIdToPay(null)
		setIsModalOpen(false)
	}

	const openModal = (e, transactionId, transactionAmount = '', actionType) => {
		setModalAmountToPay(transactionAmount)
		setModalTransactionIdToPay(transactionId)
		setModalAction(actionType)
		setSelectedTransaction(transactions.find((tr) => transactionId === tr.id))
		setIsModalOpen(true)
	}
	const closeModal = () => {
		resetState()
	}

	const payWithBundle = async (selectedFoodBundleId) => {
		event.preventDefault()
		setFormLoading(true)
		if (!selectedFoodBundleId) {
			console.log('No bundle has been selected')
			return
		}
		const selectedFoodBundle = foodBundleList.find((fb) => selectedFoodBundleId === fb.id)
		// console.log(selectedFoodBundle)
		const selectedTransaction = transactions.find((tr) => modalTransactionIdToPay === tr.id)
		// console.log(selectedTransaction)

		const bundleUpdated = { amount_spent: {} }
		const bundle_id = selectedFoodBundle.id

		// amount --
		bundleUpdated.amount = selectedFoodBundle.amount - modalAmountToPay
		if (bundleUpdated.amount < 0) bundleUpdated.amount = 0

		// Mean that amount to pay > amount left in the bundle
		let bundleAmountToAdd =
			selectedFoodBundle.amount - modalAmountToPay < 0 ? selectedFoodBundle.amount : modalAmountToPay

		// totalamountspent ++
		if (!selectedFoodBundle.amount_spent_total) selectedFoodBundle.amount_spent_total = 0
		bundleUpdated.amount_spent_total = selectedFoodBundle.amount_spent_total + bundleAmountToAdd
		bundleUpdated.amount_spent = selectedFoodBundle.amount_spent

		const transaction_month = moment(selectedTransaction.created_at)
			.utcOffset(8)
			.format('MMMM')
		const transaction_year = moment(selectedTransaction.created_at)
			.utcOffset(8)
			.year()
		// If it's a new year
		if (typeof selectedFoodBundle.amount_spent !== 'object' || !(transaction_year in selectedFoodBundle.amount_spent))
			bundleUpdated.amount_spent[transaction_year] = {}

		// if it's a new month
		if (
			typeof selectedFoodBundle.amount_spent[transaction_year] !== 'object' ||
			!(transaction_month in selectedFoodBundle.amount_spent[transaction_year])
		)
			bundleUpdated.amount_spent[transaction_year][transaction_month] = 0

		// amount_spent_by_month + add per month
		bundleUpdated.amount_spent[transaction_year][transaction_month] += bundleAmountToAdd

		// transaction ++
		if (typeof selectedFoodBundle.transactions !== 'object') {
			bundleUpdated.transactions = {}
		} else
			bundleUpdated.transactions = {
				...selectedFoodBundle.transactions,
			}
		bundleUpdated.transactions[selectedTransaction.id] = {
			order: selectedTransaction.transaction,
			created_at: selectedTransaction.created_at,
			items: selectedTransaction.items,
		}
		bundleUpdated.transactions[selectedTransaction.id].order.amount = bundleAmountToAdd

		// console.log('bundleUpdated ', bundleUpdated)
		// return setFormLoading(false)

		var ret = fetch(`/api/foodbundles/setFoodBundle?bundle_id=${bundle_id}`, {
			method: 'PATCH',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(bundleUpdated),
		})
			.then(async (ret) => {
				// console.log('Receive the answer from the server :', ret)
				const data = await ret.json()
				// console.log('Get the data from the answer :', data)
				return data
			})
			.catch((err) => {
				console.log('Something goes wrong in the function "EditBundle"', err)
				return false
			})

		if (!ret) console.log('Foodbundle has been updated')
		else {
			ret = await fetch(`/api/orders/setOrder?orderId=${modalTransactionIdToPay}`, {
				method: 'PATCH',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					paidWith: 'BUNDLE',
					paidBy: bundle_id,
				}),
			})
				.then(async (ret) => {
					// console.log('Receive the answer from the server :', ret)
					const data = await ret.json()
					// console.log('Get the data from the answer :', data)
					return data
				})
				.catch((err) => {
					console.log('Something goes wrong in the function "EditOrder"', err)
					return false
				})
		}
		if (!ret) {
			console.log('Transaction has been updated')
			setFormLoading(false)
		} else {
			setIsModalOpen(false)
			setLoading(true)
			setRefreshTransactions(true)
			setRefreshFoodBundleList(true)
			resetState()
		}
	}

	const cancelTheBundlePayment = async (selectedTransactionId) => {
		setFormLoading(true)
		if (!selectedTransactionId) return console.log('No transaction has been selected')
		// else console.log(selectedTransactionId)

		const selectedTransaction = transactions.find((tr) => selectedTransactionId === tr.id)
		// console.log('transaction', selectedTransaction)
		const selectedFoodBundle = foodBundleList.find((fb) => selectedTransaction.paidBy === fb.id)
		// console.log('foodbundle', selectedFoodBundle)

		const bundleUpdated = { amount_spent: {} }
		const bundle_id = selectedFoodBundle.id

		const amountToRefund = selectedFoodBundle.transactions[selectedTransactionId].order.amount

		// amount/credit ++
		bundleUpdated.amount = selectedFoodBundle.amount + amountToRefund
		if (bundleUpdated.amount < 0) bundleUpdated.amount = 0

		// totalamountspent --
		if (!selectedFoodBundle.amount_spent_total) selectedFoodBundle.amount_spent_total = 0
		bundleUpdated.amount_spent_total = selectedFoodBundle.amount_spent_total - amountToRefund

		const transaction_month = moment(selectedTransaction.created_at)
			.utcOffset(8)
			.format('MMMM')
		const transaction_year = moment(selectedTransaction.created_at)
			.utcOffset(8)
			.year()

		// amount_spent_by_month - amount to refund
		bundleUpdated.amount_spent = selectedFoodBundle.amount_spent
		bundleUpdated.amount_spent[transaction_year][transaction_month] -= amountToRefund

		// transaction --
		bundleUpdated.transactions = { ...selectedFoodBundle.transactions }
		delete bundleUpdated.transactions[selectedTransactionId]

		// console.log('bundleUpdated ', bundleUpdated)
		// return setFormLoading(false)

		var ret = fetch(`/api/foodbundles/setFoodBundle?bundle_id=${bundle_id}`, {
			method: 'PATCH',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(bundleUpdated),
		})
			.then(async (ret) => {
				// console.log('Receive the answer from the server :', ret)
				const data = await ret.json()
				// console.log('Get the data from the answer :', data)
				return data
			})
			.catch((err) => {
				console.log('Something goes wrong in the function "EditBundle"', err)
				return false
			})

		if (!ret) console.log('Foodbundle has been updated')
		else {
			ret = await fetch(`/api/orders/setOrder?orderId=${selectedTransactionId}`, {
				method: 'PATCH',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					paidWith: 'OTHER',
					paidBy: '',
				}),
			})
				.then(async (ret) => {
					// console.log('Receive the answer from the server :', ret)
					const data = await ret.json()
					// console.log('Get the data from the answer :', data)
					return data
				})
				.catch((err) => {
					console.log('Something goes wrong in the function "EditOrder"', err)
					return false
				})
		}
		if (!ret) {
			console.log('Transaction has been updated')
			setFormLoading(false)
		} else {
			setIsModalOpen(false)
			setLoading(true)
			setRefreshTransactions(true)
			setRefreshFoodBundleList(true)
			resetState()
		}
	}

	const updateTransactions = () => {
		setLoading(true)
		const selectedDate = moment()
		if (selectedDateDayToSub) selectedDate.subtract(selectedDateDayToSub, 'd')
		const date = selectedDate.format('YYYY-MM-DD')
		const time_start = lastTransactionTime ? lastTransactionTime : ''
		// console.log('Fetch transaction from this time :', lastTransactionTime ? lastTransactionTime : date)
		fetch(`/api/square/transferOrders?date=${date}&time_start=${time_start}`).then(async (ret) => {
			// const data = await ret.json()
			// res.json({ isOk: true, data: data })
			if (ret.ok) setRefreshTransactions(true)
		})
	}

	const addDate = () => {
		if (selectedDateDayToSub > 0) {
			setSelectedDateDayToSub(selectedDateDayToSub - 1)
			setRefreshTransactions(true)
		} else console.log('Cannot go in the future')
	}

	const subDate = () => {
		setSelectedDateDayToSub(selectedDateDayToSub + 1)
		setRefreshTransactions(true)
	}

	// console.log(modalAction)
	return (
		<div>
			<Head title="TRANSACTIONS" />
			<Nav />
			<Modal isOpen={isModalOpen} onRequestClose={closeModal} style={customStyles} contentLabel="Example Modal">
				<PayWithBundleForm
					onCancel={closeModal}
					onSave={payWithBundle}
					onRefund={cancelTheBundlePayment}
					modalAction={modalAction}
					foodBundleList={foodBundleList}
					amountToPay={modalAmountToPay}
					selectedTransaction={selectedTransaction}
					isLoading={formLoading}
				></PayWithBundleForm>
			</Modal>
			<div className="hero">
				<div className="flex justify-center items-center py-4 px-4">
					<h1 className="lg:text-5xl md:text-4xl text-center text-3xl px-2">TRANSACTIONS</h1>
				</div>
				<div className="flex justify-center pt-2 px-4">
					<button
						className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded block mx-2"
						onClick={() => updateTransactions()}
					>
						Transfer transactions
					</button>
					<DateComponent
						selectedDate={selectedDate}
						selectedDateDayToSub={selectedDateDayToSub}
						resetDate={() => {
							setSelectedDateDayToSub(0)
							setRefreshTransactions(true)
						}}
						datePrev={() => subDate()}
						dateNext={() => addDate()}
					></DateComponent>
					<button
						className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded block mx-2"
						onClick={() => {
							setRefreshTransactions(true)
							setRefreshFoodBundleList(true)
						}}
					>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
							<path
								className="heroicon-ui"
								d="M6 18.7V21a1 1 0 0 1-2 0v-5a1 1 0 0 1 1-1h5a1 1 0 1 1 0 2H7.1A7 7 0 0 0 19 12a1 1 0 1 1 2 0 9 9 0 0 1-15 6.7zM18 5.3V3a1 1 0 0 1 2 0v5a1 1 0 0 1-1 1h-5a1 1 0 0 1 0-2h2.9A7 7 0 0 0 5 12a1 1 0 1 1-2 0 9 9 0 0 1 15-6.7z"
							/>
						</svg>
					</button>
				</div>
				<div className="relative">
					{loading ? (
						<div id="overlay" className="flex justify-center">
							<div className="lds-dual-ring pt-24"></div>
						</div>
					) : (
						''
					)}
					<table className="bg-white table-auto m-auto my-4">
						<thead>
							<tr>
								<td className="border border-blue-500 px-4 py-2 text-center">PAYMENT</td>
								<td className="border border-blue-500 px-4 py-2 text-left">CREATED AT</td>
								<td className="border border-blue-500 px-4 py-2 text-center">ORDER</td>
								<td className="border border-blue-500 px-4 py-2 text-right">AMOUNT</td>
								<td className="border border-blue-500 px-4 py-2 text-center">PAY</td>
							</tr>
						</thead>
						{/* {console.log('transactions', transactions)} */}
						<tbody>
							{transactions.map((transaction) => {
								const onClickAction =
									transaction.paidWith !== 'BUNDLE'
										? (e) => openModal(e, transaction.id, transaction.transaction.amount, 'pay')
										: (e) => openModal(e, transaction.id, 0, 'refund')
								return (
									<tr
										className={'bg-white leading-7 ' + (transaction.paidBy ? 'text-gray-500' : '')}
										key={transaction.id}
										title={transaction.id}
										onClick={onClickAction}
									>
										<td className="border border-blue-200 px-4 py text-left">
											{transaction.paidWith ? transaction.paidWith : transaction.type}
										</td>
										<td className="border border-blue-200 px-4 py-2 text-center">
											{moment(transaction.created_at).format('D/M - hh:mm a')}
										</td>
										<td className="border border-blue-200 px-4 py-2">{transaction.transactionItemsString}</td>
										<td className="border border-blue-200 px-4 py-2 text-right">
											{transaction.transaction.amount.toLocaleString('en')}
										</td>
										<td className="border border-blue-200 text-center">
											{transaction.paidWith !== 'BUNDLE' ? (
												<button
													className="px-3 py-2"
													// onClick={(e) => openModal(e, transaction.id, transaction.transaction.amount, 'pay')}
												>
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
														<path
															className="heroicon-ui"
															d="M12 22a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm1-11v2h1a3 3 0 0 1 0 6h-1v1a1 1 0 0 1-2 0v-1H8a1 1 0 0 1 0-2h3v-2h-1a3 3 0 0 1 0-6h1V6a1 1 0 0 1 2 0v1h3a1 1 0 0 1 0 2h-3zm-2 0h-1a1 1 0 1 0 0 2h1V9zm2 6h1a1 1 0 0 0 0-2h-1v2z"
														/>
													</svg>
												</button>
											) : (
												// To cancel transaction
												<button
													className="px-3 py-2"
													// onClick={(e) => openModal(e, transaction.id, 'refund')}
												>
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
														<path
															className="heroicon-ui"
															d="M5 3h14a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5c0-1.1.9-2 2-2zm0 2v14h14V5H5zm8.41 7l1.42 1.41a1 1 0 1 1-1.42 1.42L12 13.4l-1.41 1.42a1 1 0 1 1-1.42-1.42L10.6 12l-1.42-1.41a1 1 0 1 1 1.42-1.42L12 10.6l1.41-1.42a1 1 0 1 1 1.42 1.42L13.4 12z"
															g=""
														/>
													</svg>
												</button>
											)}
										</td>
									</tr>
								)
							})}
						</tbody>
					</table>
				</div>
				<div className="row"></div>
			</div>
			<style jsx>{``}</style>
		</div>
	)
}

// Transactions.getInitialProps = async (ctx) => {
// 	// if (typeof window === 'undefined') console.log('server')
// 	// else console.log('client') // Happen during javascript navigation
// 	const dateNow = new Date()

// 	return { dateNow }
// }

export default Transactions
