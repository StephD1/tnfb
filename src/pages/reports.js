import React, { useEffect, useState } from 'react'
import Head from '../components/head'
import Nav from '../components/nav'
import Modal from 'react-modal'
import Moment from 'react-moment'
import moment from 'moment'
import DetailedBundle from '../components/report/DetailedBundle'
Modal.setAppElement('#__next')

const customStyles = {
	content: {
		top: '40%',
		left: '50%',
		right: '20%',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
	},
}

const bundle_type_list = ['trial', 'day', 'weekend', 'member', 'event', 'hackagu']

const FoodBundles = () => {
	const [foodbundles, setFoodbundles] = useState([])
	const [foodbundles_bak, setFoodbundles_bak] = useState([])
	const [isModalOpen, setIsModalOpen] = useState(false)
	const [refreshBundles, setRefreshBundles] = useState(true)
	const [selectedFoodBundle, setSelectedFoodBundle] = useState(null)
	const [selectedFoodBundleId, setSelectedFoodBundleId] = useState(null)
	const [selectedDate, setSelectedDate] = useState(undefined)
	const [selectedDateMonthToSub, setSelectedDateMonthToSub] = useState(0)
	const [selectedYear, setSelectedYear] = useState(moment().year())
	const [selectedMonth, setSelectedMonth] = useState(moment().format('MMMM'))
	const [totalSpent, setTotalSpent] = useState(0)

	// Show all
	const [searchedName, setSearchedName] = useState('')
	const [searchedBundleType, setSearchedBundleType] = useState(undefined)

	const [loading, setLoading] = useState(false)

	useEffect(() => {
		if (!refreshBundles) return
		async function getFoodBundles() {
			const res = await fetch(`/api/foodbundles/getFoodBundles`)
			var newObj = await res.json()
			newObj.sort((a, b) => (a.created_at > b.created_at ? -1 : a.created_at < b.created_at ? 1 : 0))

			setFoodbundles(newObj)
			setFoodbundles_bak(newObj)
			calculateTotalSpent(newObj)
			setLoading(false)
			resetState()
		}

		setLoading(true)
		getFoodBundles()
	}, [refreshBundles])

	useEffect(() => {
		// console.log('Filter bundle')
		var bundleList = []
		if (foodbundles_bak.length > 0) bundleList = foodbundles_bak

		const filteredBundleList = []
		bundleList.map((fb) => {
			if (
				(!searchedName ||
					fb.firstname.toLowerCase().includes(searchedName.toLowerCase()) ||
					fb.lastname.toLowerCase().includes(searchedName.toLowerCase())) &&
				(!searchedBundleType || fb.bundle_type === searchedBundleType)
			)
				filteredBundleList.push(fb)
		})
		// console.log('Bundle list', filteredBundleList)
		setFoodbundles(filteredBundleList)
		calculateTotalSpent(filteredBundleList)
	}, [searchedName, searchedBundleType])

	useEffect(() => {
		const localDate = moment(selectedDate)
		if (selectedDateMonthToSub) localDate.subtract(selectedDateMonthToSub, 'M')
		const selectedYear = localDate.format('YYYY')
		const selectedMonth = localDate.format('MMMM')

		setSelectedYear(selectedYear)
		setSelectedMonth(selectedMonth)
		calculateTotalSpent(null, selectedYear, selectedMonth)
	}, [selectedDateMonthToSub])

	const resetState = () => {
		setIsModalOpen(false)
		setSelectedFoodBundle(null)
		setSelectedFoodBundleId(null)
		setRefreshBundles(false)
	}

	const calculateTotalSpent = (bundleList, year = null, month = null) => {
		let totalSpent_tmp = 0
		let bundleList_tmp = foodbundles
		if (bundleList) bundleList_tmp = bundleList

		const selectedYear_tmp = year ? year : selectedYear
		const selectedMonth_tmp = month ? month : selectedMonth
		bundleList_tmp.map((b) => {
			if (b.amount_spent[selectedYear_tmp] && b.amount_spent[selectedYear_tmp][selectedMonth_tmp]) {
				totalSpent_tmp += b.amount_spent[selectedYear_tmp][selectedMonth_tmp]
			}
		})
		setTotalSpent(totalSpent_tmp)
	}

	const addDate = () => selectedDateMonthToSub > 0 && setSelectedDateMonthToSub(selectedDateMonthToSub - 1)
	const subDate = () => setSelectedDateMonthToSub(selectedDateMonthToSub + 1)

	const openModal = (e, bundle_id = null) => {
		// console.log('openModal', { e, bundle_id })
		setSelectedFoodBundleId(bundle_id)
		const foodbundle = foodbundles.filter((fb) => fb.id === bundle_id)
		setSelectedFoodBundle(foodbundle[0])
		setIsModalOpen(true)
	}
	const closeModal = () => resetState()
	const afterOpenModal = async () => {}

	return (
		<div>
			<Head title="REPORTS" />
			<Nav />
			<Modal isOpen={isModalOpen} onAfterOpen={afterOpenModal} onRequestClose={closeModal} style={customStyles}>
				<DetailedBundle onClose={closeModal} foodbundle={selectedFoodBundle}></DetailedBundle>
			</Modal>

			<div className="hero">
				<div className="flex justify-center items-center py-4 px-4">
					<div className="flex-1 justify-center"></div>
					<h1 className=" flex-1 lg:text-5xl md:text-4xl text-center text-3xl px-2 w-full">REPORTS</h1>
					<div className="flex-1 text-right">
						<p>
							TOTAL SPENT : <span className="font-bold">{totalSpent.toLocaleString('en')}</span>
						</p>
					</div>
				</div>
				<div className="flex justify-between pt-3 px-4">
					<div className="flex m-auto">
						<div className="search_name">
							<input
								className="appearance-none block mr-3 bg-white text-gray-700 border rounded py-2 px-3 leading-tight focus:outline-none"
								type="text"
								placeholder="Name or user"
								value={searchedName}
								onChange={(e) => setSearchedName(e.target.value)}
							></input>
						</div>
						<div className="bundle_type relative mr-2">
							<select
								className="block appearance-none w-full bg-gray-200 border border-blue-500 text-gray-700 py-2 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
								id="select-bundle_type"
								name="bundle_type"
								value={searchedBundleType}
								onChange={(e) => setSearchedBundleType(e.target.value)}
							>
								<option className="text-gray-600" value="">
									Pick a bundle type
								</option>
								{bundle_type_list.map((value, key) => (
									<option className="text-gray-600" key={key} value={value}>
										{value.charAt(0).toUpperCase() + value.slice(1)}
									</option>
								))}
							</select>
							<div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
								<svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
									<path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
								</svg>
							</div>
						</div>
						<div className="">
							<div className="leading-7 flex justify-end">
								<button
									className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-1 px-4 rounded-l border border-blue-500"
									onClick={() => subDate()}
								>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
										<path
											className="heroicon-ui"
											d="M14.7 15.3a1 1 0 0 1-1.4 1.4l-4-4a1 1 0 0 1 0-1.4l4-4a1 1 0 0 1 1.4 1.4L11.42 12l3.3 3.3z"
										/>
									</svg>
								</button>
								<button
									className="bg-gray-300 hover:bg-gray-400 border-t border-b border-blue-500 text-gray-800 font-bold py-1 px-4"
									onClick={() => setSelectedDateMonthToSub(0)}
								>
									<Moment format="MM-YYYY" subtract={{ month: selectedDateMonthToSub }}>
										{selectedDate}
									</Moment>
								</button>
								<button
									className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-1 px-4 rounded-r border border-blue-500"
									onClick={() => addDate()}
								>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
										<path
											className="heroicon-ui"
											d="M9.3 8.7a1 1 0 0 1 1.4-1.4l4 4a1 1 0 0 1 0 1.4l-4 4a1 1 0 0 1-1.4-1.4l3.29-3.3-3.3-3.3z"
										/>
									</svg>
								</button>
							</div>
						</div>
					</div>
				</div>
				<div className="relative">
					{loading ? (
						<div id="overlay" className="flex justify-center">
							<div className="lds-dual-ring pt-24"></div>
						</div>
					) : (
						''
					)}
					<table className="bg-white table-auto m-auto my-4">
						<thead>
							<tr>
								{/* <td className="border px-4 py-2">ID</td> */}
								<td className="border border-blue-500 px-4 py-2 text-left">FIRSTNAME</td>
								<td className="border border-blue-500 px-4 py-2 text-left">LASTNAME</td>
								<td className="border border-blue-500 px-4 py-2 text-center">TYPE</td>
								<td className="border border-blue-500 px-4 py-2 text-right">EXPIRY ON</td>
								<td className="border border-blue-500 px-4 py-2 text-right">SPENT</td>
								<td className="border border-blue-500 px-4 py-2">ACT</td>
							</tr>
						</thead>
						<tbody>
							{foodbundles.map((foodbundle) => {
								if (!foodbundle.amount_spent[selectedYear] || !foodbundle.amount_spent[selectedYear][selectedMonth])
									return
								return (
									<tr className="bg-gray-100" key={foodbundle.id} onClick={(e) => openModal(e, foodbundle.id)}>
										{/* <td className="border px-4 py-2">{foodbundle.id}</td> */}
										<td className="border border-blue-200 px-4 py-2">{foodbundle.firstname}</td>
										<td className="border border-blue-200 px-4 py-2">{foodbundle.lastname}</td>
										<td className="border border-blue-200 px-4 py-2 text-center">
											{foodbundle.bundle_type.charAt(0).toUpperCase() + foodbundle.bundle_type.slice(1)}
										</td>
										<td className="border border-blue-200 px-4 py-2 text-right">{foodbundle.expiry_date}</td>
										<td className="border border-blue-200 px-4 py-2 text-right">
											{foodbundle.amount_spent[selectedYear] && foodbundle.amount_spent[selectedYear][selectedMonth]
												? foodbundle.amount_spent[selectedYear][selectedMonth].toLocaleString('en')
												: 0}
										</td>
										<td className="border border-blue-200 px-4 py-2 text-center">
											<div
												className={`w-4 h-4 rounded m-auto ${foodbundle.active ? 'bg-green-300' : 'bg-red-300'}`}
											></div>
										</td>
									</tr>
								)
							})}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	)
}

export default FoodBundles
