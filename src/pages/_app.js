import React from 'react'
import App from 'next/app'
import Head from 'next/head'
import Router from 'next/router'

import '../../public/style/tailwindcss.css'
import '../../public/style/loadingState.css'
import Layout from '../components/Layout'
import { AuthContext } from '../components/authContext'

class MyApp extends App {
	state = {
		isAuthenticated: false,
	}

	render() {
		const { Component, pageProps } = this.props
		// console.log('props from MyApp', this.props)

		return (
			<>
				<Head>
					<title>Tropical Nomad Food Bundle Tracker</title>}
				</Head>
				<AuthContext.Provider
					value={{
						isAuthenticated: this.state.isAuthenticated,
						authenticate: (val) => {
							if (val.current.value === 'Tropical') {
								this.setState({ isAuthenticated: true })
								window.localStorage.setItem('isAuthenticated', true)
							}
						},
						unauthenticate: () => {
							this.setState({ isAuthenticated: false })
							window.localStorage.setItem('isAuthenticated', false)
						},
					}}
				>
					<Layout>
						<Component {...pageProps} />
					</Layout>
				</AuthContext.Provider>
			</>
		)
	}
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);
//
//   return { ...appProps }
// }

export default MyApp
