import React, { useEffect, useState } from 'react'
import Head from '../components/head'
import Nav from '../components/nav'
import Modal from 'react-modal'
import FoodBundlesForm from '../components/foodbundles/FoodBundlesForm'
// import Moment from 'react-moment'
Modal.setAppElement('#__next')

const customStyles = {
	content: {
		top: '40%',
		left: '50%',
		right: '20%',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
	},
}

const bundle_type_list = ['trial', 'day', 'weekend', 'member', 'event', 'hackagu']

const FoodBundles = () => {
	const [foodbundles, setFoodbundles] = useState([])
	const [foodbundles_bak, setFoodbundles_bak] = useState([])
	const [isModalOpen, setIsModalOpen] = useState(false)
	const [usersList, setUsersList] = useState(['CSO', 'StephD'])
	const [refreshBundles, setRefreshBundles] = useState(true)
	const [formAction, setFormAction] = useState(null)
	const [selectedFoodBundle, setSelectedFoodBundle] = useState(null)
	const [selectedFoodBundleId, setSelectedFoodBundleId] = useState(null)

	// Show all
	const [showInactive, setShowInactive] = useState(false)
	const [searchedName, setSearchedName] = useState('')
	const [searchedBundleType, setSearchedBundleType] = useState(undefined)

	const [loading, setLoading] = useState(false)
	const [formLoading, setFormLoading] = useState(false)

	useEffect(() => {
		if (!refreshBundles) return
		async function getFoodBundles() {
			// var orderBy = !showInactive ? '"active"' : ''
			// var filter = 'equalTo'
			// var equalTo = !showInactive ? true : ''
			// const res = await fetch(`/api/foodbundles/getFoodBundles?orderBy=${orderBy}&${filter}=${equalTo}`)

			const res = await fetch(`/api/foodbundles/getFoodBundles`)
			var newObj = await res.json()
			newObj.sort((a, b) =>
				a.firstname.toLowerCase() > b.firstname.toLowerCase()
					? 1
					: a.firstname.toLowerCase() < b.firstname.toLowerCase()
					? -1
					: 0,
			)

			setFoodbundles(newObj)
			setFoodbundles_bak(newObj)
			setLoading(false)
			filterBundleList(newObj)
			resetState()
		}
		setLoading(true)
		getFoodBundles()
	}, [refreshBundles])

	useEffect(() => {
		filterBundleList()
	}, [searchedName, searchedBundleType, showInactive])

	const filterBundleList = (bundleList_tmp = []) => {
		var bundleList = []
		if (foodbundles_bak.length > 0) bundleList = foodbundles_bak
		if (bundleList_tmp.length > 0) bundleList = bundleList_tmp

		const filteredBundleList = []
		bundleList.map((fb) => {
			if (
				(!searchedName ||
					fb.firstname.toLowerCase().includes(searchedName.toLowerCase()) ||
					fb.lastname.toLowerCase().includes(searchedName.toLowerCase())) &&
				(!searchedBundleType || fb.bundle_type === searchedBundleType)
			) {
				if (searchedName || searchedBundleType || showInactive) filteredBundleList.push(fb)
				else if (fb.active) filteredBundleList.push(fb)
			}
		})
		// console.log('Bundle list', filteredBundleList)
		setFoodbundles(filteredBundleList)
	}

	const resetState = () => {
		// setShowInactive(false)
		// setSearchedName('')
		// setSearchedBundleType('')
		setFormAction(null)
		setSelectedFoodBundle(null)
		setSelectedFoodBundleId(null)
		setRefreshBundles(false)
		setIsModalOpen(false)
		setFormLoading(false)
	}

	const disabledExpiredBundle = async () => {
		setLoading(true)
		const res = await fetch(`/api/foodbundles/disabledExpiredBundles`).then(async (ret) => {
			if (ret.ok) return true
			else return false
		})
		if (res) setRefreshBundles(true)
	}

	const openModal = (e, action = null, bundle_id = null) => {
		// console.log('openModal', { e, action, bundle_name })
		if (!action || action === 'ADD') setFormAction('ADD')
		else setFormAction('EDIT')
		if (!bundle_id) setSelectedFoodBundleId(null)
		else setSelectedFoodBundleId(bundle_id)

		setIsModalOpen(true)
	}
	const closeModal = () => {
		resetState()
	}
	const afterOpenModal = async () => {
		// console.log('AfterOpenModal', { formAction, selectedFoodBundleId })
		if (!selectedFoodBundleId) return
		// TODO : Get bundle from the state first

		const foodbundle = await fetch(`/api/foodbundles/getFoodBundle?bundle_id=${selectedFoodBundleId}`)
			.then(async (ret) => {
				// console.log('Receive the answer from the server :', ret)
				const data = await ret.json()
				// console.log('Get the data from the answer :', data)
				const data2 = data[selectedFoodBundleId]
				data2.id = selectedFoodBundleId
				// console.log('Get the data from the answer :', data2)
				if (data2) setSelectedFoodBundle(data2)
				return data2
			})
			.catch((err) => {
				console.log('Something goes wrong in the function "getStore"', err)
				return false
			})
	}

	const saveModal = async (formValues) => {
		event.preventDefault()
		setFormLoading(true)
		if (typeof formValues.amount === 'string') formValues.amount = parseInt(formValues.amount.replace(/,/g, ''))
		// console.log('saveModal', formValues)
		// return
		const ret = await fetch('/api/foodbundles/setFoodBundle', {
			method: formAction === 'ADD' ? 'POST' : 'PUT',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(formValues),
		})
			.then(async (ret) => true)
			.catch((err) => {
				console.log('Something goes wrong in the function "AddFoodBundle"', err)
				return false
			})
		if (!ret) {
			console.log('Nothing has been added')
			setFormLoading(false)
		} else {
			setIsModalOpen(false)
			setRefreshBundles(true)
		}
	}

	return (
		<div>
			<Head title="FOOD BUNDLES" />
			<Nav />
			<Modal
				isOpen={isModalOpen}
				onAfterOpen={afterOpenModal}
				onRequestClose={closeModal}
				style={customStyles}
				contentLabel="Example Modal"
			>
				<FoodBundlesForm
					onCancel={closeModal}
					onSave={saveModal}
					usersList={usersList}
					formAction={formAction}
					foodbundle={selectedFoodBundle}
					isLoading={formLoading}
				></FoodBundlesForm>
			</Modal>

			<div className="hero">
				<div className="flex justify-between items-center py-4 px-4">
					<div className="flex justify-start w-64">
						<button
							className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-2 border border-blue-500 hover:border-transparent rounded block"
							onClick={openModal}
						>
							Add bundles
						</button>
						<button
							className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded block ml-2"
							onClick={() => setRefreshBundles(true)}
						>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
								<path
									className="heroicon-ui"
									d="M6 18.7V21a1 1 0 0 1-2 0v-5a1 1 0 0 1 1-1h5a1 1 0 1 1 0 2H7.1A7 7 0 0 0 19 12a1 1 0 1 1 2 0 9 9 0 0 1-15 6.7zM18 5.3V3a1 1 0 0 1 2 0v5a1 1 0 0 1-1 1h-5a1 1 0 0 1 0-2h2.9A7 7 0 0 0 5 12a1 1 0 1 1-2 0 9 9 0 0 1 15-6.7z"
								/>
							</svg>
						</button>
					</div>
					<h1 className="lg:text-5xl md:text-4xl text-center text-3xl px-2">FOOD BUNDLES</h1>
					<div className="flex w-64 justify-end">
						<span className="w-32 text-right leading-5">Disable expired bundles</span>
						<button
							className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded block ml-2"
							onClick={() => disabledExpiredBundle()}
							title="DISABLED EXPIRED FOOD BUNDLE"
						>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
								<path
									className="heroicon-ui"
									d="M7.7 6.3L9 7.58l4.3-4.3a1 1 0 0 1 1.4 1.42L10.42 9l4.3 4.3a1 1 0 0 1-1.42 1.4L9 10.42l-1.3 1.3a3 3 0 1 1-1.4-1.42L7.58 9l-1.3-1.3a3 3 0 1 1 1.42-1.4zM21 8a1 1 0 0 1 1 1v1a1 1 0 0 1-2 0 1 1 0 0 1 0-2h1zM4 20a1 1 0 0 1 0 2H3a1 1 0 0 1-1-1v-1a1 1 0 0 1 2 0zm17 2h-1a1 1 0 0 1 0-2 1 1 0 0 1 2 0v1a1 1 0 0 1-1 1zM14 8h2a1 1 0 0 1 0 2h-2a1 1 0 0 1 0-2zm7 5a1 1 0 0 1 1 1v2a1 1 0 0 1-2 0v-2a1 1 0 0 1 1-1zm-7 7h2a1 1 0 0 1 0 2h-2a1 1 0 0 1 0-2zm-6 0h2a1 1 0 0 1 0 2H8a1 1 0 0 1 0-2zm-2.3-6.3a1 1 0 1 0-1.4-1.4 1 1 0 0 0 1.4 1.4zM5 6a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"
								/>
							</svg>
						</button>
					</div>
				</div>
				<div className="flex justify-between pt-3 leading-10 px-4">
					<div className="flex m-auto">
						<button
							className="bg-transparent hover:bg-blue-500 px-2 border border-blue-500 hover:border-transparent rounded mr-2"
							onClick={() => {
								setShowInactive(false)
								setSearchedName('')
								setSearchedBundleType('')
							}}
						>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
								<path
									className="heroicon-ui"
									d="M12 22a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zM9.56 8.93l6.37-2.12a1 1 0 0 1 1.26 1.26l-2.12 6.37a1 1 0 0 1-.63.63l-6.37 2.12a1 1 0 0 1-1.26-1.26l2.12-6.37a1 1 0 0 1 .63-.63zm-.22 5.73l4-1.33 1.32-4-4 1.34-1.32 4z"
								/>
							</svg>
						</button>
						<div className="search_name">
							<input
								className="appearance-none block mr-3 bg-white text-gray-700 border rounded py-2 px-3 leading-tight focus:outline-none"
								type="text"
								placeholder="Name or user"
								value={searchedName}
								onChange={(e) => setSearchedName(e.target.value)}
							></input>
						</div>
						<div className="bundle_type relative mr-2">
							<select
								className="block appearance-none w-full bg-gray-200 border border-blue-500 text-gray-700 py-2 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
								id="select-bundle_type"
								name="bundle_type"
								value={searchedBundleType}
								onChange={(e) => setSearchedBundleType(e.target.value)}
							>
								<option className="text-gray-600" value="">
									Pick a bundle type
								</option>
								{bundle_type_list.map((value, key) => (
									<option className="text-gray-600" key={key} value={value}>
										{value.charAt(0).toUpperCase() + value.slice(1)}
									</option>
								))}
							</select>
							<div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
								<svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
									<path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
								</svg>
							</div>
						</div>
						<div
							className="ml-2 active_or_not self-center flex h-8 items-center"
							onClick={() => setShowInactive(!showInactive)}
						>
							<input
								className="mr-2 transform scale-150"
								type="checkbox"
								onChange={() => setShowInactive(!showInactive)}
								checked={showInactive === true}
							></input>
							<span className="text-sm pointer-events-none select-none">Show inactive</span>
						</div>
					</div>
				</div>
				<div className="relative">
					{loading ? (
						<div id="overlay" className="flex justify-center">
							<div className="lds-dual-ring pt-24"></div>
						</div>
					) : (
						''
					)}
					<table className="bg-white table-auto m-auto my-4">
						<thead>
							<tr>
								{/* <td className="border px-4 py-2">ID</td> */}
								<td className="border border-blue-500 px-4 py-2 text-left">FIRSTNAME</td>
								<td className="border border-blue-500 px-4 py-2 text-left">LASTNAME</td>
								<td className="border border-blue-500 px-4 py-2 text-center">TYPE</td>
								<td className="border border-blue-500 px-4 py-2 text-right">EXPIRE ON</td>
								<td className="border border-blue-500 px-4 py-2 text-right">CREDIT</td>
								<td className="border border-blue-500 px-4 py-2 text-right">SPENT</td>
								<td className="border border-blue-500 px-2 py-2 text-center">ACTIVE</td>
								<td className="border border-blue-500 px-2 py-2 text-center" hidden>
									OP
								</td>
							</tr>
						</thead>
						<tbody>
							{foodbundles.map((foodbundle) => {
								return (
									<tr
										className={`bg-white leading-7 ${foodbundle.active ? '' : 'text-gray-500'}`}
										key={foodbundle.id}
										onClick={(e) => openModal(e, 'EDIT', foodbundle.id)}
									>
										{/* <td className="border px-4 py-2">{foodbundle.id}</td> */}
										<td className="border border-blue-200 px-4 py-2">{foodbundle.firstname}</td>
										<td className="border border-blue-200 px-4 py-2">{foodbundle.lastname}</td>
										<td className="border border-blue-200 px-4 py-2 text-center">
											{foodbundle.bundle_type.charAt(0).toUpperCase() + foodbundle.bundle_type.slice(1)}
										</td>
										<td className="border border-blue-200 border-r-0 px-4 py-2 text-right text-sm">
											{foodbundle.expiry_date}
										</td>
										<td className="border border-blue-300 px-4 py-2 text-right bg-blue-200 font-bold">
											{foodbundle.amount && foodbundle.amount.toLocaleString('en')}
										</td>
										<td className="border border-blue-200 px-4 py-2 text-right">
											{foodbundle.amount_spent_total && foodbundle.amount_spent_total.toLocaleString('en')}
										</td>
										<td className="border border-blue-200 px-4 py-2 text-center">
											<div
												className={`w-4 h-4 rounded m-auto ${foodbundle.active ? 'bg-green-300' : 'bg-red-300'}`}
											></div>
										</td>
										<td className="border text-center" hidden>
											<button className="px-3 py-2" onClick={(e) => openModal(e, 'EDIT', foodbundle.id)}>
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
													<path
														className="heroicon-ui"
														d="M6.3 12.3l10-10a1 1 0 0 1 1.4 0l4 4a1 1 0 0 1 0 1.4l-10 10a1 1 0 0 1-.7.3H7a1 1 0 0 1-1-1v-4a1 1 0 0 1 .3-.7zM8 16h2.59l9-9L17 4.41l-9 9V16zm10-2a1 1 0 0 1 2 0v6a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h6a1 1 0 0 1 0 2H4v14h14v-6z"
													/>
												</svg>
											</button>
										</td>
									</tr>
								)
							})}
						</tbody>
					</table>
				</div>
				<div className="row"></div>
			</div>
		</div>
	)
}

export default FoodBundles
