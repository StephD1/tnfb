import React, { useEffect, useState } from 'react'
import Head from '../components/head'
import Nav from '../components/nav'
import { AuthContext } from '../components/authContext'

const Home = () => {
	const [date, setDate] = useState(null)
	const { isAuthenticated, authenticate, unauthenticate } = React.useContext(AuthContext)
	const password = React.useRef(null)

	useEffect(() => {
		async function getDate() {
			const res = await fetch('/api/date')
			const newDate = await res.json()
			setDate(newDate)
		}
		getDate()
	}, [])

	return (
		<>
			<Head title="Home" />
			<Nav />

			<div className="hero">
				<h1 className="title">Welcome !</h1>
				<div>
					<p className="text-center">
						{date ? (
							<span>
								<b>{date.date}</b>
							</span>
						) : (
							<span className="loading"></span>
						)}
					</p>
				</div>
				<div className="pt-10">
					{!isAuthenticated ? (
						<form className="pt-5 flex items-center flex-col flex-1">
							<label className="text-xl">Please connect first</label>
							<input
								className="appearance-none block bg-gray-200 text-gray-700 border border-blue-500 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white m-2"
								placeholder="password"
								type="password"
								autoComplete="off"
								ref={password}
							></input>
							<button
								className="bg-transparent hover:bg-blue-500 text-black font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded block m-2"
								onClick={() => authenticate(password)}
							>
								Connect
							</button>
						</form>
					) : (
						<>
							<div className="pt-5 flex items-center flex-col flex-1">
								<button
									className="bg-transparent hover:bg-blue-500 text-black font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded block m-2"
									onClick={() => unauthenticate()}
								>
									Disconnect
								</button>
							</div>
						</>
					)}
				</div>
			</div>
		</>
	)
}

export default Home
