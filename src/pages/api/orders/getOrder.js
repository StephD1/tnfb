import fetch from 'isomorphic-unfetch'

module.exports = async (req, res) => {
	let orderId = req.query.orderId
	if (!orderId) res.send({ msg: 'No orderID in the query' })

	try {
		if (req.method === 'GET') {
			await fetch(`https://tnfb-84dae.firebaseio.com/transactions/${orderId}.json`)
				.then((ret) => {
					console.log('getTransactions:', ret.ok)
					if (ret.ok) return ret.json()
				})
				.then((data) => {
					// console.log('data', data)
					res.json({ isOk: true, order: data })
				})
				.catch((e) => {
					console.log('Something goes wrong in the function "getOrder"', e)
					// return []
				})
		} else res.status(400).send({ err: true, msg: 'Incorrect request method' })
	} catch (error) {
		console.log('Something wrong happen:', error.messages)
	}
}
