import fetch from 'isomorphic-unfetch'

export default async (req, res) => {
	// console.log('query', req.query)
	// console.log('body', req.body)
	let transactionId = req.query.orderId
	if (!transactionId) res.send({ msg: 'No orderID in the query' })

	try {
		if (req.method === 'GET') {
			res.status(400).send({ isOk: false, err: true, msg: 'Cannot perform a SET on a GET request' })
		} else if (req.method === 'PATCH') {
			const transaction_data = req.body
			// console.log({ transactionId, transaction_data })
			const ret = await fetch(`https://tnfb-84dae.firebaseio.com/transactions/${transactionId}.json`, {
				method: 'PATCH', // normal that its a patch (Allow to change only one value)
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(transaction_data),
			})
				.then(async (ret) => {
					// console.log('Receive the answer from the server :', ret)
					const data = await ret.json()
					// console.log('Get the data from the answer :', data)
					res.json(data)
				})
				.catch((err) => {
					console.log('Something goes wrong in the function "EditTransaction"')
					return false
				})
		} else res.status(400).send({ err: true, msg: 'Incorrect request method' })
	} catch (error) {
		console.log('error catched', error)
		res.status(404).send({ err: true, msg: 'Unknow error', error })
	}
}
