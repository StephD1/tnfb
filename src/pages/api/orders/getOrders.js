import fetch from 'isomorphic-unfetch'
import moment from 'moment'

export default async (req, res) => {
	var date = req.query.date ?? null
	var dateNeeded = null
	if (date) dateNeeded = moment(date).format('YYYY-MM-DD')
	var queryParams = ''
	if (dateNeeded) {
		queryParams = queryParams + `?orderBy="created_at_date"`
		queryParams = queryParams + `&equalTo="${dateNeeded}"`
	}

	await fetch('https://tnfb-84dae.firebaseio.com/transactions.json' + queryParams)
		.then((res) => {
			// console.log('res.ok', res.ok)
			// Become this as a log
			// 2020-01-30T05:40:24.346Z	94b59b04-4e01-4678-b65d-2eccc4f1c11e	INFO	res.ok true
			if (res.ok) return res.json()
		})
		.then((data) => {
			if (!data) {
				res.json([])
				return
			}
			const arrayOfObj = Object.keys(data).map((key) => {
				return data[key]
			})
			// console.log('data1', arrayOfObj.length)
			var dataFiltered = []
			if (dateNeeded) {
				// Will fitler the array
				// console.log('Get the transaction of this day', dateNeeded)
				dataFiltered = arrayOfObj.filter((d) => {
					// console.log(moment(d.created_at).format('YYYY-MM-DD'))
					return (
						dateNeeded ===
						moment(d.created_at)
							.utcOffset(8)
							.format('YYYY-MM-DD')
					)
				})
				dataFiltered.forEach((data) => {
					let transactionItemsString = ''
					data.items.slice(0, 2).forEach((item) => {
						transactionItemsString += item.name + '(' + item.quantity + '), '
					})
					transactionItemsString = transactionItemsString.substr(0, transactionItemsString.length - 2)
					if (data.items.length > 2) transactionItemsString += ' ...'
					data.transactionItemsString = transactionItemsString
				})
				// console.log('data2', dataFiltered)
			}
			res.json(dataFiltered)
		})
		.catch((e) => {
			console.log('e', e)
			// return []
		})
}
