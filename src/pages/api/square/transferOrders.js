import fetch from 'isomorphic-unfetch'
import moment from 'moment'
import SquareConnect from 'square-connect'

var locationId = '2GREVZ6VEF1YT' // String | The ID of the location to list transactions for.
var tokenId = 'EAAAEIkapvF7fUTbp5HqNbsmSVAC_nXrnPwe8_wxTpMk17fASZhNG-RsJPEPM8Gp'
var defaultClient = SquareConnect.ApiClient.instance

const transactionObj = {
	id: '',
	location_id: '',
	created_at: '',
	type: '',
	order: {
		id: '',
		amount: '',
	},
}

const orderObj = {
	id: '',
	location_id: '',
	created_at: '',
	type: '',
	transaction: {
		id: '',
		amount: '',
		type: '',
	},
	items: [],
}

const squareToTransaction = (squareTransaction) => {
	// console.log(squareTransaction);
	const transaction = { ...transactionObj }
	transaction.id = squareTransaction.id
	transaction.location_id = squareTransaction.location_id
	transaction.created_at = squareTransaction.created_at
	transaction.created_at_date = moment(squareTransaction.created_at)
		.utcOffset(8)
		.format('YYYY-MM-DD')
	transaction.created_at_time = moment(squareTransaction.created_at)
		.utcOffset(8)
		.format('HH:MM')
	let paymentType = squareTransaction.tenders[0].type
	if (paymentType === 'THIRD_PARTY_CARD') paymentType = 'CARD'
	transaction.type = paymentType
	transaction.order = { ...transactionObj.order }
	transaction.order.id = squareTransaction.tenders[0].id
	transaction.order.amount = squareTransaction.tenders[0].amount_money.amount / 100

	return transaction
}

const squareToOrder = (squareOrder) => {
	const order = { ...orderObj }
	order.id = squareOrder.id
	order.location_id = squareOrder.location_id
	order.created_at = squareOrder.created_at
	order.created_at_date = moment(squareOrder.created_at)
		.utcOffset(8)
		.format('YYYY-MM-DD')
	order.created_at_time = moment(squareOrder.created_at)
		.utcOffset(8)
		.format('HH:MM')
	let paymentType = squareOrder.tenders[0].type
	if (paymentType === 'THIRD_PARTY_CARD') paymentType = 'CARD'
	order.type = paymentType

	order.transaction = { ...orderObj.transaction }
	order.transaction.id = squareOrder.tenders[0].id
	order.transaction.amount = squareOrder.tenders[0].amount_money.amount / 100
	order.transaction.type = squareOrder.tenders[0].type

	order.items = []
	if (squareOrder.line_items && squareOrder.line_items.length > 0) {
		squareOrder.line_items.forEach((item) => {
			order.items.push({ name: item.name, quantity: item.quantity, base_price: item.base_price_money.amount })
		})
	}
	return order
}

const getTransactions = async (date_start = null, date_end = null) => {
	if (!date_start || !date_end) return
	var oauth2 = defaultClient.authentications['oauth2']
	oauth2.accessToken = tokenId
	var apiInstance = new SquareConnect.TransactionsApi()

	// YYYY-MM-DDT00:00:00Z
	var opts = {
		beginTime: `${date_start}`,
		endTime: `${date_end}`,
		// sortOrder: "",  (`ASC` for oldest first, `DESC` for newest first).  Default value: `DESC`
		cursor: '',
	}
	// console.log('opts', opts)

	// Because square work with UTC and we are +8
	opts.beginTime = moment(opts.beginTime)
		.subtract(8, 'h')
		.toISOString()
	opts.endTime = moment(opts.endTime)
		.subtract(8, 'h')
		.toISOString()

	var cursorTmp = null
	var transactions = []

	do {
		opts.cursor = cursorTmp
		await apiInstance.listTransactions(locationId, opts).then((data) => {
			// console.log('data length', data.transactions.length)
			// console.log('data cursor', data.cursor)
			if (data.transactions) transactions = transactions.concat(data.transactions)

			if (data.cursor) cursorTmp = data.cursor
			else cursorTmp = null

			return data.transactions
		})
	} while (cursorTmp)
	return transactions
}

const getOrders = async (date_start = null, date_end = null) => {
	if (!date_start || !date_end) return
	var oauth2 = defaultClient.authentications['oauth2']
	oauth2.accessToken = tokenId
	var apiInstance = new SquareConnect.OrdersApi()
	var body = new SquareConnect.SearchOrdersRequest()
	var cursorTmp = null
	var orders = []

	body.location_ids = ['2GREVZ6VEF1YT']
	body.query = {
		filter: {
			date_time_filter: {
				created_at: {
					start_at: moment(date_start)
						.subtract(8, 'h')
						.toISOString(),
					end_at: moment(date_end)
						.subtract(8, 'h')
						.toISOString(),
				},
				// created_at: {
				// 	start_at: '2020-02-12T07:10:26Z',
				// 	end_at: '2020-02-13T07:10:26Z',
				// },
			},
		},
	}
	// console.log(body.query.filter.date_time_filter)

	do {
		body.cursor = cursorTmp
		await apiInstance
			.searchOrders(body)
			.then((data) => {
				// console.log('data length', data.orders.length)
				// console.log('data cursor', data.cursor)
				if (data.orders) orders = orders.concat(data.orders)
				if (data.cursor) cursorTmp = data.cursor
				else cursorTmp = null
				// return data.orders
			})
			.catch((err) => {
				console.log('Error', err.text)
			})
	} while (cursorTmp)
	return orders
}

export default async (req, res) => {
	// var day = req.query.day ?? ''
	var date_start = req.query.date_start ?? ''
	var date_end = req.query.date_end ?? ''
	var time_start = req.query.time_start ?? ''
	if (req.query.date) date_start = date_end = req.query.date
	// Redo time start if it's there
	if (time_start) {
		date_start = moment(time_start)
			.add(8, 'h')
			.subtract(1, 'm')
			.toISOString()
	} else date_start = date_start + 'T00:00:00Z'
	date_end = date_end + 'T23:59:59Z'
	console.log('Transfer orders : date', date_start)

	// console.log('Get transactions from SquareUp')
	// const transactions = await getTransactions(date_start, date_end)
	console.log('Get orders from SquareUp')
	const transactions = await getOrders(date_start, date_end)
	// console.log('total transactions', transactions)
	if (transactions.length === 0) return res.json({ isOk: false, msg: 'There is no transaction from square' })
	else console.log('total transactions', transactions.length)

	// Filter the list (THIRD_PARTY_CARD && CASH)
	let transactionList = transactions.filter((t) => {
		if (!t.tenders) return
		if (t.tenders.length > 1) t.tenders = t.tenders.filter((t2) => t2.type !== 'THIRD_PARTY_CARD' && t2.type !== 'CASH')
		if (t.tenders.length === 1) return t.tenders[0].type !== 'THIRD_PARTY_CARD' && t.tenders[0].type !== 'CASH'
	})
	// console.log('total transactions', transactionList[0])
	// console.log('First transaction', squareToOrder(transactionList[0]))
	// return res.json({})

	// PUSH transaction to firebase
	if (transactionList.length === 0) console.log('Nothing to push to Firebase')
	else console.log(`Push ${transactionList.length} transactions to Firebase`)
	// transactionList.forEach((transaction) => {
	const transactionsList = []
	await Promise.all(
		transactionList.map(async (transaction) => {
			let transactionTmp = squareToOrder(transaction)
			try {
				// Try with axios
				await fetch(`https://tnfb-84dae.firebaseio.com/transactions/${transactionTmp.id}.json`, {
					method: 'PATCH',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify(transactionTmp),
				})
					.then(async (ret) => {
						// console.log('Receive the answer from firebase :', ret.ok)
						if (ret.ok) transactionsList.push(transactionTmp)
						// const data = await ret.json()
						// console.log('Get the data from the answer :', { id: data.created_at })
						return true
					})
					.catch((err) => {
						console.log('Error with axios', err.toString())
						// return false
					})
			} catch (err) {
				console.log('error with fetch')
			}
		}),
	)

	console.log(`Job is done with ${transactionsList.length} success`)
	res.json({ isOk: true })
}
