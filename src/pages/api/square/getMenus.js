import fetch from 'isomorphic-unfetch'
// import moment from 'moment'

// Square data
import SquareConnect from 'square-connect'
// var locationId = '2GREVZ6VEF1YT'
var tokenId = 'EAAAEIkapvF7fUTbp5HqNbsmSVAC_nXrnPwe8_wxTpMk17fASZhNG-RsJPEPM8Gp'
var defaultClient = SquareConnect.ApiClient.instance

const menuItem = {
	id: '',
	location_id: '',
	updated_at: '',
	is_deleted: '',
	name: '',
	category_id: '',
	item_data: {
		abbreviation: '',
		ordinal: '',
		price: '',
		visibility: '',
		variations: [],
	},
}

const menuCategory = {
	id: '',
	updated_at: '',
	name: '',
	is_deleted: false,
}

const squareToMenuCategoryObj = (squareMenuCategObj) => {
	const category = { ...menuCategory }

	category.id = squareMenuCategObj.id
	category.name = squareMenuCategObj.category_data.name
	category.updated_at = squareMenuCategObj.updated_at
	category.is_deleted = squareMenuCategObj.is_deleted

	return category
}
const convertSquareMenuCategories = (squareMenuCategories) => {
	const listOfCategories = {}
	console.log('Nb of categories to convert:', squareMenuCategories.length)

	if (squareMenuCategories.length > 0) {
		squareMenuCategories.forEach((category) => {
			if (category.type === 'CATEGORY') listOfCategories[category.id] = squareToMenuCategoryObj(category)
		})
	}

	delete listOfCategories['3GBSSGHVJ4ZYBSKIEAMI6HYJ']

	return listOfCategories
}

const squareToMenuObj = (squareMenuObj) => {
	const item = { ...menuItem }

	item.id = squareMenuObj.id
	// item.location_id = squareMenuObj.present_at_location_ids[0]
	item.is_deleted = squareMenuObj.is_deleted
	item.updated_at = squareMenuObj.updated_at
	item.name = squareMenuObj.item_data.name
	item.category_id = squareMenuObj.item_data.category_id

	item.item_data = { ...menuItem.item_data }
	item.item_data.abbreviation = squareMenuObj.item_data.abbreviation
	// item.item_data.ordinal = squareMenuObj.ordinal
	item.item_data.visibility = squareMenuObj.item_data.visibility

	if (squareMenuObj.item_data.variations[0].item_variation_data.price_money !== undefined)
		item.item_data.price = squareMenuObj.item_data.variations[0].item_variation_data.price_money.amount
	else item.item_data.price = undefined

	if (squareMenuObj.item_data.variations.length === 1) item.item_data.variations = undefined
	else {
		item.item_data.variations = new Array()
		squareMenuObj.item_data.variations.forEach((item_variation) => {
			item.item_data.variations.push({
				id: item_variation.id,
				name: item_variation.item_variation_data.name,
				ordinal: item_variation.item_variation_data.ordinal,
				price: item_variation.item_variation_data.price_money.amount,
			})
		})
	}

	return item
}
const convertSquareMenuItems = (squareMenuItems) => {
	const listOfItems = {}
	console.log('Nb of items to convert:', squareMenuItems.length)

	if (squareMenuItems.length > 0) {
		squareMenuItems.forEach((item) => {
			if (item.type === 'ITEM') listOfItems[item.id] = squareToMenuObj(item)
		})
	}

	return listOfItems
}

const getMenuCategories = async () => {
	console.log('getMenuCategories')
	var oauth2 = defaultClient.authentications['oauth2']
	oauth2.accessToken = tokenId
	var apiInstance = new SquareConnect.CatalogApi()
	var opts = {
		cursor: '',
		types: 'CATEGORY',
	}
	var cursorTmp = null
	var categories = []

	do {
		opts.cursor = cursorTmp
		await apiInstance
			.listCatalog(opts)
			.then((data) => {
				// console.log('API called successfully. Returned data: ' + data.objects.length)
				if (data.objects) categories = categories.concat(data.objects)
				if (data.cursor) cursorTmp = data.cursor
				else cursorTmp = null
			})
			.catch((err) => {
				console.error(err)
			})
	} while (cursorTmp)
	return categories
}
const pushCategoriesToFirebase = async (categories) => {
	console.log('pushCategoriesToFirebase')

	const res = await fetch(`https://tnfb-dev.firebaseio.com/menu/categories.json`, {
		method: 'PATCH',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify(categories),
	})

	if (!res.ok) console.log('Something goes wrong with pushCategoriesToFirebase')
	return res
}

const getMenuItems = async () => {
	console.log('getMenuItems')
	var oauth2 = defaultClient.authentications['oauth2']
	oauth2.accessToken = tokenId
	var apiInstance = new SquareConnect.CatalogApi()
	var opts = {
		cursor: '',
		types: 'ITEM',
	}
	var cursorTmp = null
	var items = []

	do {
		opts.cursor = cursorTmp
		await apiInstance
			.listCatalog(opts)
			.then((data) => {
				// console.log('API called successfully. Returned data: ' + data.objects.length)
				if (data.objects) items = items.concat(data.objects)
				if (data.cursor) cursorTmp = data.cursor
				else cursorTmp = null
			})
			.catch((err) => {
				console.error(err)
			})
	} while (cursorTmp)
	return items
}
const pushItemsToFirebase = async (items) => {
	console.log('pushItemsToFirebase')

	const res = await fetch(`https://tnfb-dev.firebaseio.com/menu/items.json`, {
		method: 'PATCH',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify(items),
	})

	if (!res.ok) console.log('Something goes wrong with pushItemsToFirebase')
	return res
}

export default async (req, res) => {
	console.log('From getMenus')

	// Get categories
	var squareMenuCategories = await getMenuCategories()
	const categories = convertSquareMenuCategories(squareMenuCategories)
	var ret = await pushCategoriesToFirebase(categories)

	// Get Items
	var squareMenuItems = await getMenuItems()
	const items = convertSquareMenuItems(squareMenuItems)
	ret = await pushItemsToFirebase(items)

	res.json({ isOk: true, data: { categories, items } })
}
