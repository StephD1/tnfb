import fetch from 'isomorphic-unfetch'

export default async (req, res) => {
	// console.log('query', req.query)
	// console.log('body', req.body)
	var bundle_id = req.query.bundle_id
	try {
		if (req.method === 'GET') {
			// get a bundle
			// console.log('bundle_id', bundle_id)
			await fetch(`https://tnfb-84dae.firebaseio.com/food-bundles/${bundle_id}.json`)
				.then((ret) => {
					// console.log('GetFoodBundle:', ret.ok)
					if (ret.ok) return ret.json()
				})
				.then((data) => {
					// console.log('data', data)
					res.json({ [bundle_id]: { ...data } })
				})
				.catch((e) => {
					console.log('Something goes wrong in the function "GetBundle"', e)
					// return []
				})
		} else res.status(400).send({ err: true, msg: 'Incorrect request method' })
	} catch (error) {
		console.log('error catched', error)
		res.status(404).send({ err: true, msg: 'Unknow error', error })
	}
}
