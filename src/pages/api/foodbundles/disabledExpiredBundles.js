import fetch from 'isomorphic-unfetch'
import moment from 'moment'

export default async (req, res) => {
	// var start = new Date().getTime()
	// console.log('Execution time: ' + (new Date().getTime() - start))

	// Get all bundle
	let foodBundles
	try {
		foodBundles = await fetch(`https://tnfb-84dae.firebaseio.com/food-bundles.json?orderBy="active"&equalTo=true`)
			.then(async (ret) => {
				// console.log('Receive the answer from the server :', ret.ok)
				if (ret.ok) return await ret.json()
				else throw 'err'
			})
			.catch((err) => {
				console.log('Error with fetch', err.toString())
				return false
			})
	} catch (err) {
		console.log('error in fetch foodbundle', err)
	}

	if (!foodBundles) {
		res.send('Cannot get foodbundles')
		return
	}
	// Object to Array
	const arrayOfBundles = Object.keys(foodBundles).map((key) => {
		// data[key].name = key
		foodBundles[key].id = key
		return foodBundles[key]
	})

	const bundleToDisabled = []
	const dateOfToday = moment()
		.utcOffset(0)
		.set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
	arrayOfBundles.forEach((fb) => {
		let dateOfBundle = moment(fb.expiry_date, 'DD/MM/YYYY')
		if (process.env.NODE_ENV === 'development') dateOfBundle.add(8, 'h')
		// if (fb.firstname === 'Stephane') {
		// console.log('bundle', dateOfBundle.toISOString())
		// console.log('today', dateOfToday.toISOString())
		if (dateOfBundle <= dateOfToday) {
			// console.log('before')
			bundleToDisabled.push(fb)
		}
		// if (dateOfBundle > dateOfToday) console.log('after')
		// if (dateOfBundle.isSame(dateOfToday)) console.log('same')
		// }
	})

	// console.log('nb to disable', bundleToDisabled.length)
	const body_to_write = {}
	bundleToDisabled.forEach((b) => {
		body_to_write[b.id + '/active'] = false
		if (b.bundle_type !== 'member' && b.amount > 0) body_to_write[b.id + '/amount'] = 0
	})
	// console.log(body_to_write)
	// return res.send({})
	const bundleDisabled = []
	const val = await fetch(`https://tnfb-84dae.firebaseio.com/food-bundles.json`, {
		method: 'PATCH',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify(body_to_write),
	})
		.then(async (ret) => {
			// console.log('Receive the answer from the server :', ret.ok)
			const data = await ret.json()
			// console.log('Get the data from the answer :', data)
			for (var key in data) bundleDisabled.push(key)
			return true
		})
		.catch((err) => {
			console.log('Something goes wrong in the function "disableBundle"')
			return false
		})
	// console.log('nb   disabled', bundleDisabled.length)
	res.json({ bundleDisabled })
}
