import fetch from 'isomorphic-unfetch'
import moment from 'moment'

export default async (req, res) => {
	// console.log('query', req.query)
	// console.log('body', req.body)
	try {
		if (req.method === 'GET') {
			res.status(400).send({ isOk: false, err: true, msg: 'Cannot perform a SET on a GET request' })
		} else if (req.method === 'PUT' || req.method === 'POST') {
			// PUT will update, POST will create
			const req_method = req.method
			// console.log('ADD : ' + [req.method], req.body)
			const bundle_data = req.body
			const bundle_id = req.body.id
			const url =
				req_method === 'POST'
					? 'https://tnfb-84dae.firebaseio.com/food-bundles.json'
					: `https://tnfb-84dae.firebaseio.com/food-bundles/${bundle_id}.json`

			if (req_method === 'POST') {
				bundle_data.date_created = moment()
					.utcOffset(8)
					.format('DD/MM/YYYY, HH:mm')
				const current_month = moment()
					.utcOffset(8)
					.format('MMMM')
				const current_year = moment()
					.utcOffset(8)
					.year()
				bundle_data.amount_spent[current_year] = {}
				bundle_data.amount_spent[current_year][current_month] = 0
			}

			bundle_data.date_updated = moment()
				.utcOffset(8)
				.format('DD/MM/YYYY, HH:mm')

			delete bundle_data.id
			bundle_data.amount = parseInt(bundle_data.amount)
			// console.log(`bundle_data : ${bundle_id}`, bundle_data)
			// return
			const ret = await fetch(url, {
				method: req_method, // normal that its a put (Allow to have a custom ID)
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(bundle_data),
			})
				.then(async (ret) => {
					// console.log('Receive the answer from the server :', ret.ok)
					const data = await ret.json()
					// console.log('Get the data from the answer :', data)
					res.json({ isOk: true, data: data })
				})
				.catch((err) => {
					console.log('Something goes wrong in the function "AddBundle"')
					return false
				})
		} else if (req.method === 'PATCH') {
			// edit a bundle precisly, just the value that we want to edit
			const bundle_data = req.body
			const bundle_id = req.query.bundle_id
			bundle_data.date_updated = moment()
				.utcOffset(8)
				.format('DD/MM/YYYY, HH:mm')
			// console.log({ bundle_id, b: bundle_data.amount_spent })
			// return
			const ret = await fetch(`https://tnfb-84dae.firebaseio.com/food-bundles/${bundle_id}.json`, {
				method: 'PATCH', // normal that its a patch (Allow to change only one value)
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(bundle_data),
			})
				.then(async (ret) => {
					// console.log('Receive the answer from the server :', ret)
					const data = await ret.json()
					// console.log('Get the data from the answer :', data)
					res.json(data)
				})
				.catch((err) => {
					console.log('Something goes wrong in the function "EditBundle"')
					return false
				})
		} else res.status(400).send({ err: true, msg: 'Incorrect request method' })
	} catch (error) {
		console.log('error catched', error)
		res.status(404).send({ err: true, msg: 'Unknow error', error })
	}
}
