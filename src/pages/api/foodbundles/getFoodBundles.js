import fetch from 'isomorphic-unfetch'

export default async (req, res) => {
	try {
		if (req.method === 'GET') {
			var { orderBy, equalTo } = req.query
			// console.log({ orderBy, equalTo, startAt, endAt })
			var queryParams = ''
			if (orderBy) queryParams = queryParams + `?orderBy=${orderBy}`
			if (equalTo) queryParams = queryParams + `&equalTo=${equalTo}`
			// get a bundle
			await fetch(`https://tnfb-84dae.firebaseio.com/food-bundles.json${queryParams}`)
				.then((ret) => {
					console.log('getFoodBundles isOk', ret.ok)
					if (ret.ok) return ret.json()
				})
				.then((data) => {
					if (!data) data = []
					// console.log('data', data)
					const arrayOfObj = Object.keys(data).map((key) => {
						// data[key].name = key
						data[key].id = key
						return data[key]
					})
					// console.log('arrayOfObj', arrayOfObj)
					res.json(arrayOfObj)
				})
				.catch((e) => {
					console.log('Something goes wrong in the function "GetFoodBundles"', e)
					// return []
				})
		} else res.status(400).send({ err: true, msg: 'Incorrect request method' })
	} catch (error) {
		console.log('error catched', error)
		res.status(404).send({ err: true, msg: 'Unknow error', error })
	}
}

// SAVING THIS INFO ON THE SIDE

// const res = await fetch(url.rest, {
// 	method: 'POST',
// 	headers: { 'Content-Type': 'application/json' },
// 	body: JSON.stringify(body.rest),
// })
// const data = await res.json()
// fetch("/api/auth/signout", { method: "POST" });

// Axios.get("https://tnfb-84dae.firebaseio.com/transactions.json").then(
//   orders => {
//     console.log(orders.data);
//   }
// );
