const transactionObj = {
	id: '',
	location_id: '',
	created_at: '',
	order: {
		id: '',
		type: '',
		amount: '',
	},
}

const squareToObject = (squareTransaction) => {
	const transactionObj = { ...transactionObj }
	transactionObj.id = squareTransaction.id
	transactionObj.location_id = squareTransaction.location_id
	transactionObj.created_at = squareTransaction.created_at
	transactionObj.order.id = squareTransaction.tenders[0].id
	transactionObj.order.type = squareTransaction.tenders[0].type
	transactionObj.order.amount = squareTransaction.tenders[0].amount_money.amount

	return transactionObj
}

export default transactionObj
