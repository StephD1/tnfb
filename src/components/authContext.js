import React from 'react'

const store = {
	isAuth: false,
}

export const AuthContext = React.createContext(store)
