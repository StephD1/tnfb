import React, { useState } from 'react'
import moment from 'moment'
import FoodBundlesFormInput from './FoodBundlesFormInput'

const defaultFormValues = {
	firstname: '',
	lastname: '',
	email: '',
	bundle_type: 'trial',
	amount: 0,
	amount_spent_total: 0,
	amount_spent: {},
	transactions: [],
	expiry_date: moment()
		.add(1, 'd')
		.format('DD/MM/YYYY')
		.toString(),
	active: true,
}

const bundle_type_list = ['trial', 'day', 'weekend', 'member', 'event', 'hackagu']

export default function FoodBundlesForm(props) {
	const [formValues, setFormValues] = React.useState(defaultFormValues)
	// const [nameAlreadyExist, setNameAlreadyExist] = useState(false)
	const [isLoading, setIsLoading] = useState(false)
	// console.log(formValues)

	React.useEffect(() => {
		// console.log('foodbundle', props.foodbundle)
		// console.log('useEffect', props.formAction)
		if (props.foodbundle) {
			setFormValues(props.foodbundle)
			setIsLoading(false)
		} else if (props.formAction === 'EDIT') setIsLoading(true)
	}, [props.foodbundle])

	React.useEffect(() => {
		if (props.isLoading && props.formAction === 'EDIT') setIsLoading(props.isLoading)
	}, [props.isLoading])

	React.useEffect(() => {
		if (props.formAction === 'EDIT' && formValues === null) setIsLoading(true)
	}, [])

	const updateValues = (e) => {
		e.preventDefault()
		let target = e.target.name
		let value = e.target.value
		let expiry_date = formValues.expiry_date
		// console.log('values before', { target, value })

		if (target === 'bundle_type') {
			value = value.toLowerCase()
			expiry_date = moment().utcOffset(8)
			if (value !== 'member')
				expiry_date = moment(expiry_date)
					.add(1, 'd')
					.format('DD/MM/YYYY')
					.toString()
			else
				expiry_date = moment(expiry_date)
					.add(1, 'M')
					.add(1, 'd')
					.format('DD/MM/YYYY')
					.toString()
		}
		if (target === 'active') value = value === 'false'
		if (target === 'amount_plus') {
			target = 'amount'
			let amount = parseInt(value * 1000)
			if (formValues && formValues.amount) amount += parseInt(formValues.amount)
			value = parseInt(amount)
		}
		if (target === 'expiry_date') expiry_date = value

		// console.log('values after', { target, value, expiry_date })
		const newForm = {
			...formValues,
			[target]: value,
			expiry_date: expiry_date,
		}
		setFormValues(newForm)
	}

	const buttonActiveColor = formValues && formValues.active ? 'bg-green-300' : 'bg-red-300'

	// TODO : Adding more control when entering input. To avoid duplicated data
	return (
		<div>
			<h2 className="pb-2">FOOD BUNDLE FORM</h2>
			{isLoading ? (
				<div id="overlay" className="flex justify-center items-center absolute">
					<div className="lds-dual-ring"></div>
				</div>
			) : (
				''
			)}
			<form className="w-full">
				<div className="flex flex-wrap -mx-3 mb-1">
					<FoodBundlesFormInput
						name="firstname"
						value={formValues ? formValues.firstname : undefined}
						onChange={(e) => updateValues(e)}
					/>
					<FoodBundlesFormInput
						name="lastname"
						value={formValues ? formValues.lastname : undefined}
						onChange={(e) => updateValues(e)}
					/>
					<FoodBundlesFormInput
						name="email"
						value={formValues ? formValues.email : undefined}
						onChange={(e) => updateValues(e)}
					/>
				</div>
				<div className="flex flex-wrap -mx-3 mb-1">
					<FoodBundlesFormInput
						name="phone WA"
						value={formValues ? formValues.phone : undefined}
						onChange={(e) => updateValues(e)}
					/>
					<FoodBundlesFormInput
						name="amount"
						value={formValues ? formValues.amount.toLocaleString('en') : undefined}
						onChange={(e) => updateValues(e)}
					/>
					<div className="w-full md:w-1/3 px-3">
						<label className="block uppercase tracking-wide text-gray-700 text-xs font-bold" htmlFor="grid-password">
							MODIFY AMOUNT
						</label>
						<div className="flex md:justify-between leading-7">
							<button
								className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white p-2 border border-blue-500 hover:border-transparent rounded block mr-2 w-full"
								type="button"
								name="amount_plus"
								value="50"
								onClick={(e) => updateValues(e)}
							>
								50
							</button>
							<button
								className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white p-2 border border-blue-500 hover:border-transparent rounded block mr-2 w-full"
								type="button"
								name="amount_plus"
								value="90"
								onClick={(e) => updateValues(e)}
							>
								90
							</button>
							<button
								className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white p-2 border border-blue-500 hover:border-transparent rounded block mr-2 w-full"
								type="button"
								name="amount_plus"
								value="100"
								onClick={(e) => updateValues(e)}
							>
								100
							</button>
							<button
								className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white p-2 border border-blue-500 hover:border-transparent rounded block w-full"
								type="button"
								name="amount_plus"
								value="500"
								onClick={(e) => updateValues(e)}
							>
								500
							</button>
						</div>
					</div>
				</div>
				<div className="flex flex-wrap -mx-3 mb-2">
					<div className="w-full md:w-1/3 px-3">
						<label className="block uppercase tracking-wide text-gray-700 text-xs font-bold" htmlFor="grid-last-name">
							BUNDLE TYPE
						</label>

						<div className="relative">
							<select
								className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
								id="select-bundle_type"
								name="bundle_type"
								value={formValues ? formValues.bundle_type : undefined}
								onChange={(e) => updateValues(e)}
							>
								{bundle_type_list.map((value, key) => (
									<option className="text-gray-600" key={key} value={value}>
										{value.charAt(0).toUpperCase() + value.slice(1)}
									</option>
								))}
							</select>
							<div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
								<svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
									<path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
								</svg>
							</div>
						</div>
					</div>
					<FoodBundlesFormInput
						name="expiry_date"
						value={formValues ? formValues.expiry_date : undefined}
						onChange={(e) => updateValues(e)}
					/>
					<div className="w-full md:w-1/3 px-3 mb-1 md:mb-0">
						<label className="block uppercase tracking-wide text-gray-700 text-xs font-bold" htmlFor="grid-zip">
							ACTIVE
						</label>
						<div className="relative self-center flex justify-between">
							{/* <label className="switch">
									<input
										type="checkbox"
										name="active"
										value={formValues && formValues.active ? true : false}
										checked={formValues && formValues.active === true}
										onChange={(e) => updateValues(e)}
									></input>
									<span className="slider round"></span>
								</label> */}
							<span className={'text-sm self-center p-4 leading-4 rounded' + ' ' + buttonActiveColor}>
								{formValues && formValues.active ? 'Active' : 'Not active'}
							</span>
							<button
								className="text-grey-700 p-2 border border-blue-500 hover:border-gray-800 rounded block px-4 "
								name="active"
								value={formValues && formValues.active ? true : false}
								onClick={(e) => updateValues(e)}
							>
								{formValues && formValues.active ? 'DISABLE' : 'ENABLE'}
							</button>
						</div>
					</div>
				</div>
				<div className="actions flex w-full px-3 mb-1 md:mb-0">
					<button
						className="bg-transparent hover:bg-blue-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded block m-auto"
						type="button"
						onClick={props.onCancel}
					>
						CANCEL
					</button>
					<button
						className="bg-transparent hover:bg-blue-500 text-green-500 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded block m-auto"
						type="button"
						onClick={() => props.onSave(formValues)}
					>
						SAVE
					</button>
				</div>
			</form>
		</div>
	)
}
