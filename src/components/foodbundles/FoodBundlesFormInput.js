import React, { useState } from 'react'

export default function FoodBundlesFormInput(props) {
	return (
		<div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
			<label className="block uppercase tracking-wide text-gray-700 text-xs font-bold" htmlFor="grid-first-name">
				{props.name.replace(/[_\s]+/g, ' ')}
			</label>
			<input
				className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
				id={'grid-first-' + props.name}
				type={props.type ? props.type : 'text'}
				placeholder={[props.name]}
				name={props.name}
				value={props.value}
				onChange={props.onChange}
			/>
			{props.isValie && props.isValid === false && (
				<p className="text-red-500 text-xs italic">Please fill out this field.</p>
			)}
		</div>
	)
}
