import React from 'react'
import Moment from 'react-moment'
import moment from 'moment'

const DateComponent = (props) => {
	// console.log('Date Component rendered')
	// console.log(props)

	return (
		<div className="leading-relaxed border border-blue-500 rounded mr-2">
			<button
				className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l"
				onClick={props.datePrev}
			>
				Prev
			</button>
			<button
				className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 border-l border-r border-blue-500"
				onClick={props.resetDate}
			>
				{/* {props.selectedDate.format('DD/MM-YYYY')} */}
				<Moment format="DD/MM-YYYY" subtract={{ day: props.selectedDateDayToSub }}>
					{props.selectedDate}
				</Moment>
			</button>
			<button
				className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r"
				onClick={props.dateNext}
			>
				Next
			</button>
		</div>
	)
}

export default DateComponent
