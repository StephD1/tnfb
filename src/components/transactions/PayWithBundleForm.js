import React, { useState, useEffect } from 'react'
import Select from 'react-select'

const select_custrom_style = {
	menu: () => ({
		position: 'absolute',
		background: 'white',
		borderWidth: '1px',
		borderColor: '#a0aec0',
	}),
	container: () => ({
		lineHeight: '1.75rem',
	}),
}

export default function PayWithBundleForm(props) {
	const [foodBundlesList, setFoodBundlesList] = useState([])
	const [selectedFoodBundleId, setSelectedFoodBundleId] = useState(null)
	const [amountLeftToPay, setAmountLeftToPay] = useState(0)
	const [modalAction, setModalAction] = useState(null)
	const [isLoading, setIsLoading] = useState(false)
	const [refundData, setRefundData] = useState({
		transaction_id: '',
		bundle_id: '',
		refundAmount: 0,
		bundleAmoutLeft: 0,
	})

	let transactionItemsString = ''

	useEffect(() => {
		const bundleList = []
		props.foodBundleList.forEach((fb) => {
			if (
				(props.modalAction === 'pay' && fb.amount > 0) ||
				(props.modalAction === 'refund' && fb.id === props.selectedTransaction.paidBy)
			)
				bundleList.push({
					label:
						fb.firstname + ' ' + fb.lastname + ' (' + fb.bundle_type + ')' + ' / ' + fb.amount.toLocaleString('en'),
					value: fb.id,
				})
		})

		setFoodBundlesList(bundleList)
	}, [props.foodBundleList])

	React.useEffect(() => setIsLoading(props.isLoading), [props.isLoading])

	useEffect(() => {
		if (props.selectedTransaction.paidBy) {
			const data = { ...refundData }
			data.transaction_id = props.selectedTransaction.id
			data.bundle_id = props.selectedTransaction.paidBy

			const bundle = props.foodBundleList.find((b) => b.id === data.bundle_id)
			if (bundle) {
				data.refundAmount = bundle.transactions[data.transaction_id].order.amount
				data.bundleAmoutLeft = bundle.amount + data.refundAmount
			}
			setRefundData(data)
		}
	}, [props.selectedTransaction])

	useEffect(() => setModalAction(props.modalAction), [props.modalAction])

	const updateSelectedBundle = (selectedbundle) => {
		setSelectedFoodBundleId(selectedbundle.value)

		// Calculate amount left to pay
		const bundle = foodBundlesList.find((b) => b.value === selectedbundle.value)
		const parts = bundle.label.split('/')
		const amountInBundle = parseInt(parts[1].replace(/,/g, ''))
		const amountLeft = amountInBundle - props.amountToPay
		if (amountLeft < 0) setAmountLeftToPay(Math.abs(amountLeft))
		else setAmountLeftToPay(0)
	}

	if (props.selectedTransaction.items)
		props.selectedTransaction.items.forEach((item) => {
			transactionItemsString += item.name + '(' + item.quantity + '), '
		})

	transactionItemsString = transactionItemsString.substr(0, transactionItemsString.length - 2)
	const bundleIndex = foodBundlesList.findIndex((fb) => fb.value === refundData.bundle_id)

	return (
		<div>
			<h2>FOOD BUNDLE {modalAction === 'pay' ? 'CHOICE' : 'REFUND'}</h2>
			{isLoading ? (
				<div id="overlay" className="flex justify-center items-center absolute">
					<div className="lds-dual-ring"></div>
				</div>
			) : (
				''
			)}
			<form className="w-full" action="">
				<div className="flex flex-wrap -mx-3 my-3">
					<div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
						<label className="block uppercase tracking-wide text-gray-700 text-xs font-bold" htmlFor="grid-first-name">
							You will have to {modalAction}
						</label>
						<div className="">
							<p className="text-gray-900 bg-gray-200 text-gray-700 border border-gray-400 rounded py-2 px-4">
								{props.amountToPay ? props.amountToPay.toLocaleString() : refundData.refundAmount.toLocaleString()}
							</p>
						</div>
					</div>
					<div className="w-full md:w-2/3 px-3">
						<label className="block uppercase tracking-wide text-gray-700 text-xs font-bold" htmlFor="grid-last-name">
							ITEMS
						</label>
						<div className="h-10 items-center flex">
							<span>{transactionItemsString}</span>
						</div>
					</div>
				</div>

				<div className="flex flex-wrap -mx-3 my-3">
					<div className="w-full md:w-2/3 px-3">
						<label className="block uppercase tracking-wide text-gray-700 text-xs font-bold" htmlFor="grid-last-name">
							{modalAction === 'pay' ? 'FOOD BUNDLE' : 'TO'}
						</label>
						<div className="">
							<Select
								value={foodBundlesList[bundleIndex]}
								onChange={modalAction === 'pay' ? updateSelectedBundle : () => {}}
								options={foodBundlesList}
								styles={select_custrom_style}
							></Select>
						</div>
					</div>
					<div className="w-full md:w-1/3 px-3">
						<label className="block uppercase tracking-wide text-gray-700 text-xs font-bold" htmlFor="grid-last-name">
							{modalAction === 'pay' ? 'Left to pay' : 'New bundle amount'}
						</label>
						<div className="	">
							<p className="text-gray-900 bg-gray-200 text-gray-700 border border-gray-400 rounded py-2 px-4">
								{modalAction === 'pay' ? amountLeftToPay.toLocaleString() : refundData.bundleAmoutLeft.toLocaleString()}
							</p>
						</div>
					</div>
				</div>
				<div className="actions flex w-full px-3 mb-6 md:mb-0 pt-2">
					<button
						className="bg-transparent hover:bg-blue-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded block m-auto"
						type="button"
						onClick={props.onCancel}
					>
						CANCEL
					</button>
					{modalAction === 'pay' ? (
						<button
							className="bg-transparent hover:bg-blue-500 text-green-500 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded block m-auto"
							type="button"
							onClick={() => props.onSave(selectedFoodBundleId)}
						>
							SAVE
						</button>
					) : (
						<button
							className="bg-transparent hover:bg-blue-500 text-orange-500 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded block m-auto"
							type="button"
							onClick={() => props.onRefund(props.selectedTransaction.id)}
						>
							REFUND
						</button>
					)}
				</div>
			</form>
		</div>
	)
}
