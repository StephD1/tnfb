import React from 'react'
import Link from 'next/link'
import { AuthContext } from '../components/authContext'

const links = [
	{ href: '/transactions', label: 'TRANSACTIONS' },
	{ href: '/foodbundles', label: 'FOOD BUNDLES' },
	{ href: '/reports', label: 'REPORTS' },
	// { href: '/users', label: 'USERS' },
].map((link) => {
	link.key = `nav-link-${link.href}-${link.label}`
	return link
})

const Nav = () => {
	const { isAuthenticated } = React.useContext(AuthContext)

	return (
		<nav className="nav">
			<ul className="bg-blue-200 flex h-16 jsx-2194296366 nav_list p-0">
				{/* <li className="nav_item flex-auto jsx-2194296366 justify-center hover:bg-blue-300">
					<Link href="/">
						<a className="self-center container py-5 text-blue-900 text-xl">HOME</a>
					</Link>
				</li> */}

				{
					/*true || */ isAuthenticated &&
						links.map(({ key, href, label }) => (
							<li className="nav_item flex-1 jsx-2194296366 justify-center hover:bg-blue-300" key={key}>
								<Link href={href}>
									<a className="self-center container py-5 text-blue-900 text-xl">{label}</a>
								</Link>
							</li>
						))
				}
			</ul>

			<style jsx>{`
				:global(body) {
					margin: 0;
					font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir, Helvetica, sans-serif;
				}
				nav {
					text-align: center;
				}
				ul {
					display: flex;
					justify-content: space-between;
				}
				nav > ul {
					padding: 4px 16px;
				}
				li {
					display: flex;
					padding: 6px 8px;
				}
				a {
					color: #067df7;
					text-decoration: none;
					font-size: 13px;
				}
			`}</style>
		</nav>
	)
}

export default Nav
