import React from 'react'
import { withRouter, Router } from 'next/router'
import { AuthContext } from '../components/authContext'

const Layout = (props) => {
	const { router, children } = props
	const { isAuthenticated, authenticate, unauthenticate } = React.useContext(AuthContext)

	// TODO : Make it in the server side, using cookie or firebase auth
	if (typeof window !== 'undefined') {
		const formTmp = window.localStorage.getItem('isAuthenticated')
		const isAuth = JSON.parse(formTmp)
		if (!isAuthenticated && isAuth) authenticate({ current: { value: 'Tropical' } })
		else if (!isAuthenticated && router.pathname !== '/') router.replace('/')
	}

	return <div className="bg-gray-100 min-h-screen">{children}</div>
}

export default withRouter(Layout)
