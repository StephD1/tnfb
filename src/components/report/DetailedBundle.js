import React from 'react'
import moment from 'moment'

export default function DetailedBundle(props) {
	const transactions = props.foodbundle ? props.foodbundle.transactions : {}
	let totalAmountSpent = 0
	if (transactions)
		Object.keys(transactions).map((key) => {
			let t = transactions[key]
			totalAmountSpent += t.order.amount
		})

	return (
		<div className="">
			<div>Total amount spent = {totalAmountSpent}</div>
			<table className="m-auto my-5">
				<thead>
					<tr>
						<td className="border px-4 py-2">DATE</td>
						<td className="border px-4 py-2">ORDER</td>
						<td className="border px-4 py-2">PRICE</td>
					</tr>
				</thead>
				<tbody>
					{transactions &&
						Object.keys(transactions).map((key) => {
							let t = transactions[key]
							let transactionItemsString = ''
							if (t.items && t.items.length > 0)
								t.items.slice(0, 2).forEach((item) => {
									transactionItemsString += item.name + '(' + item.quantity + '), '
								})
							transactionItemsString = transactionItemsString.substr(0, transactionItemsString.length - 2)
							if (t.items && t.items.length > 2) transactionItemsString += ' ...'
							return (
								<tr key={key}>
									<td className="border px-4 py-2">{moment(t.created_at).format('DD-MM-YYYY, hh:mm a')}</td>
									<td className="border px-4 py-2">{transactionItemsString}</td>
									<td className="border px-4 py-2">{t.order.amount.toLocaleString()}</td>
								</tr>
							)
						})}
				</tbody>
			</table>
			<button
				className="bg-transparent hover:bg-blue-500 text-blue-500 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded block m-auto"
				type="button"
				onClick={() => props.onClose()}
			>
				Close
			</button>
		</div>
	)
}
