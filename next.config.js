// next.config.js
// const withPlugins = require("next-compose-plugins");
const withCSS = require('@zeit/next-css')
// const withPurgeCss = require('next-purgecss')

// Enable withPurgeCss when deploy
// const plugins = [withCSS] //, withPurgeCss]

// class TailwindExtractor {
// 	static extract(content) {
// 		return content.match(/[A-Za-z0-9-_:\/]+/g) || []
// 	}
// }

// const nextConfig = {
// purgeCssEnabled: ({ dev, isServer }) => !dev && !isServer, // Only enable PurgeCSS for client-side production builds
// purgeCss: {
// 	whitelist: ['html', 'body'],
// 	extractors: [
// 		{
// 			extractor: TailwindExtractor,
// 			extensions: ['html', 'js', 'css', 'tsx'],
// 		},
// 	],
// },
// }

module.exports = withCSS({})
// module.exports = withCss(withPurgeCss())
