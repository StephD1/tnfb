module.exports = {
	important: true,
	theme: {
		screens: {
			sm: '640px',
			md: '768px',
			lg: '1024px',
			xl: '1280px',
		},
		fontFamily: {
			display: ['Gilroy', 'sans-serif'],
			body: ['Graphik', 'sans-serif'],
		},
		borderWidth: {
			default: '1px',
			'0': '0',
			'2': '2px',
			'4': '4px',
		},
		extend: {
			colors: {
				'bc-btn': '#9cdbff',
			},
			spacing: {
				'96': '24rem',
				'128': '32rem',
			},
			height: {
				'100': '25rem',
			},
			maxHeight: {
				'100': '25rem',
			},
		},
		container: {
			center: true,
		},
	},
	variants: {},
	plugins: [],
	// purge: ['./src/**/*.html', './src/**/*.jsx'],
}
