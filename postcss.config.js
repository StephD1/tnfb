const tailwindcss = require('tailwindcss')
const autoprefixer = require('autoprefixer')
const purgecss = require('@fullhuman/postcss-purgecss')({
	content: ['./src/**/*.html', './src/**/*.jsx', './src/**/*.js'],
	defaultExtractor: (content) => content.match(/[\w-/:]+(?<!:)/g) || [],
	// defaultExtractor: (content) => content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || [],
	// defaultExtractor: (content) => content.match(/[A-Za-z0-9-_:/]+/g) || [],
})
const cssnano = require('cssnano')

module.exports = (ctx) => ({
	plugins: [
		tailwindcss('./tailwind.config.js'),
		autoprefixer,
		ctx.env === 'production' ? purgecss : null,
		ctx.env === 'production' ? cssnano({ preset: 'default' }) : null,
	],
})
